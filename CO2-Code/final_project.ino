// constants won't change. They're used here to
// set pin numbers:
const int buttonPin1 = 19;    // the number of the pushbutton pin
const int motorPin =  18;      // the number of the LED pin 1
const int ledPin2 =  17;      // the number of the LED pin 2
const int ledPin3 =  15;      // the number of the LED pin 3
const int ledPin4 =  14;      // the number of the LED pin 4
const int buttonPin2 = 13;    // the number of the pushbutton pin

// variables will change:
int breadboardButtonState1 = 0; // variable for reading the BB button status
int breadboardButtonState2 = 1; // variable for reading the BB reset button status
int lightstate = 0;

/* In the setup we will set our LEDs to OUTPUT and our buttons
 * to INPUT_PULLUP
 */
void setup()
{
  // initialize the breadboard LED pins as an output:
  pinMode(motorPin, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(ledPin4, OUTPUT);
  // initialize the breadboard push button pin as an input:
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);

}
/* In the loop section we going to get the state of the pin
 * using digitalRead() and save that to our variables
 * if the button is pressed we turn the LED on otherwise
 * the LED is off.
 */
void loop()
{
  // read the state of the push button value:
  breadboardButtonState1 = digitalRead(buttonPin1);

  while( breadboardButtonState1 != 0 )
  {
    breadboardButtonState1 = digitalRead(buttonPin1);
    breadboardButtonState2 = digitalRead(buttonPin2);
    if( breadboardButtonState2 == 0 )
    {
      breadboardButtonState1 = 1;
      break;
    }
  }

  if( (breadboardButtonState1 == 0) && (!lightstate) )
  {
    digitalWrite(motorPin, HIGH); // turn motor on

    digitalWrite(ledPin2, HIGH); // turn LED on:
    digitalWrite(ledPin3, HIGH); // turn LED on:
    digitalWrite(ledPin4, HIGH); // turn LED on:
    lightstate = 1;
  }
  else
  {
    digitalWrite(motorPin, LOW);  // turn motor off:
    digitalWrite(ledPin2, LOW);   // turn LED off:
    digitalWrite(ledPin3, LOW);   // turn LED off:
    digitalWrite(ledPin4, LOW);   // turn LED off:
    lightstate = 0;
  }

  delay(300);
}
